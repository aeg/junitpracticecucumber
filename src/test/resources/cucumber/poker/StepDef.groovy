import cucumber.poker.Card
import cucumber.poker.Pat
import cucumber.poker.PokerGame

this.metaClass.mixin(cucumber.api.groovy.Hooks)
this.metaClass.mixin(cucumber.api.groovy.EN)

Given(~'^I got ([SHDC])(\\d+),([SHDC])(\\d+),([SHDC])(\\d+),([SHDC])(\\d+),([SHDC])(\\d+) cards with groovy$') {
  char suit1, int no1,
  char suit2, int no2,
  char suit3, int no3,
  char suit4, int no4,
  char suit5, int no5 ->
    sut = new PokerGame()
    sut.setUp(Card.get(suit1, no1),
            Card.get(suit2, no2),
            Card.get(suit3, no3),
            Card.get(suit4, no4),
            Card.get(suit5, no5))
}

When(~'^i don\'t change my cards with groovy$') {->
  sut.noChange();
}

Then(~'^no pair with groovy$') {->
  Pat result = sut.pat()
  result == Pat.NO_PAIR
}

Then(~'^one pair of (\\d+) with groovy$') { int no ->
  Pat expected = new Pat.OnePair(no);
  sut.pat() == expected
}