# language: en
#機能:  ポーカーゲーム
#  シナリオ: ノーチェンジ/ノーペア（役なし）
#    前提 手札にS1,H4,D6,D8,C3が配られた
#    もし チェンジしない
#    ならば ノーペアであるべき

Feature:  Poker Game Groovy Test Ver.

  Scenario: NoChange
    Given I got S1,H4,D6,D8,C3 cards with groovy
    When  i don't change my cards with groovy
    Then  no pair with groovy

  Scenario: NoChange,1 pair
    Given I got S1,H1,D6,D8,C3 cards with groovy
    When  i don't change my cards with groovy
    Then one pair of 1 with groovy

#  Scenario: NoChange,2 pairs
#    Given I got S1,H1,D6,H6,C3 cards with groovy
#    When i don't change my cards with grovy
#    Then two pairs of 1 and 6 with groovy