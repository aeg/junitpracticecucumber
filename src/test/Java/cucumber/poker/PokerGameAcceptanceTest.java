package cucumber.poker;

import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

/**
 * User: aeg
 * Date: 2013/04/14
 * Time: 0:52
 */
@RunWith(Cucumber.class)
@Cucumber.Options(format = { "pretty"}, monochrome = true)
public class PokerGameAcceptanceTest {
}
