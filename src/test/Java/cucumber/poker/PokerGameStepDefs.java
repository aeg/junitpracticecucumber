package cucumber.poker;


import cucumber.api.java.ja.前提;
import cucumber.api.java.ja.もし;
import cucumber.api.java.ja.ならば;

import cucumber.poker.Pat.*;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class PokerGameStepDefs {

    PokerGame sut;

    @前提("^I got ([SHDC])(\\d+),([SHDC])(\\d+),([SHDC])(\\d+),([SHDC])(\\d+),([SHDC])(\\d+) cards$")
    public void I_got_5_cards(
            char suit1, int no1,
            char suit2, int no2,
            char suit3, int no3,
            char suit4, int no4,
            char suit5, int no5) {
        sut = new PokerGame();
        sut.setUp(Card.get(suit1, no1),
                Card.get(suit2, no2),
                Card.get(suit3, no3),
                Card.get(suit4, no4),
                Card.get(suit5, no5));

    }

    @もし("^i don't change my cards$")
    public void i_dont_change_my_cards() throws Throwable {
        sut.noChange();
    }

    @ならば("^no pair$")
    public void no_pair() throws Throwable {
        Pat result = sut.pat();
        assertThat(result, is(Pat.NO_PAIR));
    }

    @ならば("^one pair of (\\d+)$")
    public void one_pair_of(int no) throws Throwable {
        Pat expected = new OnePair(no);
        assertThat(sut.pat(), is (expected));
    }

}
