# language: en
#機能:  ポーカーゲーム
#  シナリオ: ノーチェンジ/ノーペア（役なし）
#    前提 手札にS1,H4,D6,D8,C3が配られた
#    もし チェンジしない
#    ならば ノーペアであるべき
Feature: Poker Game

  Scenario: NoChange
    Given I got S1,H4,D6,D8,C3 cards
    When  i don't change my cards
    Then  no pair

  Scenario: NoChange,1 pair
    Given I got S1,H1,D6,D8,C3 cards
    When  i don't change my cards
    Then one pair of 1
#
#  Scenario: NoChange,2 pair
#    Given I got S1,H1,D6,H6,C3 cards
#    When i don't change my cards
#    Then two pair of 1 and 6