package cucumber.poker;

import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * User: aeg
 * Date: 2013/04/14
 * Time: 11:14
 */
public class Hands implements Iterable<Card> {

    private static final int SIZE = 5;
    final Set<Card> cards = new HashSet<Card>(SIZE);

    public Hands(Card... cards) {
        if (cards.length != SIZE) throw new IllegalStateException();
        Collections.addAll(this.cards, cards);
/*        for (Card card : cards) {
            this.cards.add(card);
        }*/
    }

    @Override
    public Iterator<Card> iterator() {
        return cards.iterator();
    }
}
